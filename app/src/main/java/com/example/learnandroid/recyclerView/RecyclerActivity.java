package com.example.learnandroid.recyclerView;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.example.learnandroid.R;

import java.util.ArrayList;
import java.util.List;

public class RecyclerActivity extends Activity implements PersonAdapter.ItemClicked {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    //hardcoded data
    List<Person> mPersonList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recycler_view);

        mRecyclerView = (RecyclerView) findViewById(R.id.custom_recycler);
        mRecyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        //layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        //layoutManager = new GridLayoutManager(this, 1, LinearLayoutManager.VERTICAL,false);


        mRecyclerView.setLayoutManager(layoutManager);

        mPersonList = new ArrayList<>();
        mPersonList.add(new Person("Alexander", "Golovinov", Person.Preference.BUS));
        mPersonList.add(new Person("Pavel", "Golovinov", Person.Preference.PLANE));
        mPersonList.add(new Person("Irina", "Fesko", Person.Preference.PLANE));
        mPersonList.add(new Person("Boris", "Gus", Person.Preference.BUS));
        mPersonList.add(new Person("Alexander", "Golovinov", Person.Preference.BUS));
        mPersonList.add(new Person("Pavel", "Golovinov", Person.Preference.PLANE));
        mPersonList.add(new Person("Irina", "Fesko", Person.Preference.PLANE));
        mPersonList.add(new Person("Boris", "Gus", Person.Preference.BUS));
        mPersonList.add(new Person("Alexander", "Golovinov", Person.Preference.BUS));
        mPersonList.add(new Person("Pavel", "Golovinov", Person.Preference.PLANE));
        mPersonList.add(new Person("Irina", "Fesko", Person.Preference.PLANE));
        mPersonList.add(new Person("Boris", "Gus", Person.Preference.BUS));
        mPersonList.add(new Person("Alexander", "Golovinov", Person.Preference.BUS));
        mPersonList.add(new Person("Pavel", "Golovinov", Person.Preference.PLANE));
        mPersonList.add(new Person("Irina", "Fesko", Person.Preference.PLANE));
        mPersonList.add(new Person("Boris", "Gus", Person.Preference.BUS));
        mPersonList.add(new Person("Alexander", "Golovinov", Person.Preference.BUS));
        mPersonList.add(new Person("Pavel", "Golovinov", Person.Preference.PLANE));
        mPersonList.add(new Person("Irina", "Fesko", Person.Preference.PLANE));
        mPersonList.add(new Person("Boris", "Gus", Person.Preference.BUS));
        mPersonList.add(new Person("Alexander", "Golovinov", Person.Preference.BUS));
        mPersonList.add(new Person("Pavel", "Golovinov", Person.Preference.PLANE));
        mPersonList.add(new Person("Irina", "Fesko", Person.Preference.PLANE));
        mPersonList.add(new Person("Boris", "Gus", Person.Preference.BUS));

        //Set adapter to RecyclerView. Adapter will feed the data to the View.
        //The Context(this) reffered to the current activity. We can reuse it in the PersonAdapter class and reuse it there.
        mAdapter = new PersonAdapter(this, mPersonList);
        mRecyclerView.setAdapter(mAdapter);
    }


    @Override
    public void itemClicked(int position) {
        Toast.makeText(this, "Surname: " + mPersonList.get(position).getLastname(), Toast.LENGTH_SHORT).show();
    }
}
