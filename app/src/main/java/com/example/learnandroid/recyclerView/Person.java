package com.example.learnandroid.recyclerView;

public class Person {

    private String name;
    private String lastname;
    private Preference preferences;

    public Person(String name, String lastname, Preference preferences) {
        this.name = name;
        this.lastname = lastname;
        this.preferences = preferences;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Preference getPreferences() {
        return preferences;
    }

    public void setPreferences(Preference preferences) {
        this.preferences = preferences;
    }

    public enum Preference {
        BUS, PLANE
    }
}
