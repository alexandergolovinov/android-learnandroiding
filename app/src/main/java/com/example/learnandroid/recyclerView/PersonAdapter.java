package com.example.learnandroid.recyclerView;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.learnandroid.R;

import java.util.List;

//The custom adapter extends "RecyclerView.Adapter" and will use specific (PersonAdapter.ViewHolder) ViewHolder to populate data
public class PersonAdapter extends RecyclerView.Adapter<PersonAdapter.MyCustomViewHolder> {

    private  ItemClicked itemClickedActivity;

    //Data that Adapter will use
    private  List<Person> mPeopleData;

    public interface ItemClicked {
        void itemClicked(int position);
    }

    //Context is an Activity
    public PersonAdapter(Context context, List<Person> list) {
        itemClickedActivity = (ItemClicked) context;
        mPeopleData = list;
    }

    /**
     * VIEW HOLDER Pattern
     * Provide a reference to the views for each data item.
     * You provide access to all the view components for a data item in a view holder
     */
    public static class MyCustomViewHolder extends RecyclerView.ViewHolder {
        //Define all items in your custom signle-item list
        ImageView mImageView;
        TextView mName;
        TextView mSurname;

        //Create constructor
        public MyCustomViewHolder(View itemView) {
            super(itemView);
            //List all of them to the view.
            mImageView = itemView.findViewById(R.id.image_view);
            mName = itemView.findViewById(R.id.item_name);
            mSurname = itemView.findViewById(R.id.item_lastname);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Do when the item is clicked
                }
            });

        }
    }

    @NonNull
    @Override // Create new views (invoked by the layout manager)
    public PersonAdapter.MyCustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //This VIEW is reffered to the custom single-list item you have created
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_items, parent, false);
        //Now every element from custom ViewHolder is reffered to the items in R.layout.list_items
        return new MyCustomViewHolder(view);
    }

    /**
     * Called by RecyclerView to display the data at the specified position.
     * This method should update the contents of the itemView to reflect the item at the given position.
     *
     * @param viewHolder custom View Holder which has the View as an input
     * @param position   of the element in the list
     */
    @Override
    public void onBindViewHolder(@NonNull PersonAdapter.MyCustomViewHolder viewHolder, int position) {

        viewHolder.mImageView.setTag(mPeopleData.get(position));
        viewHolder.mName.setText(mPeopleData.get(position).getName());
        viewHolder.mSurname.setText(mPeopleData.get(position).getLastname());

        if (mPeopleData.get(position).getPreferences().equals(Person.Preference.BUS)) {
            viewHolder.mImageView.setImageResource(R.drawable.bus);
        } else {
            viewHolder.mImageView.setImageResource(R.drawable.plane);
        }
    }

    @Override
    public int getItemCount() {
        return mPeopleData.size();
    }
}
