package com.example.learnandroid.fragmentsAndRecycler;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.learnandroid.R;

public class ActivityDisplay extends FragmentActivity implements UserAdapter.ItemClicked {

    private TextView mUserName;
    private TextView mUserTelephone;
    private EditText mEnterUserName;
    private EditText mEnterUserPhone;
    private Button mAddPerson;

    private ListFrag mListFrag;
    FragmentManager fragmentManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);

        mUserName = findViewById(R.id.user_name);
        mUserTelephone = findViewById(R.id.user_tel);
        mEnterUserName = findViewById(R.id.enter_user_name);
        mEnterUserPhone = findViewById(R.id.enter_user_phone);
        mAddPerson = findViewById(R.id.btn_add_user);

        fragmentManager = this.getSupportFragmentManager();
        mListFrag = (ListFrag) fragmentManager.findFragmentById(R.id.list_frag);

        mAddPerson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userName = mEnterUserName.getText().toString();
                String userPhone = mEnterUserPhone.getText().toString();

                if (TextUtils.isEmpty(userName) || TextUtils.isEmpty(userPhone)) {
                    Toast.makeText(getApplicationContext(), "All firelds should be filled", Toast.LENGTH_SHORT).show();
                    return;
                }
                Toast.makeText(getApplicationContext(), "Contact succesfully added!", Toast.LENGTH_SHORT).show();
                addUser(userName, userPhone);
                mListFrag.notifyDataChanged();
            }
        });
        onItemClicked(0);
    }

    @Override
    public void onItemClicked(int index) {
        mUserName.setText(ApplicationClass.people.get(index).getName().trim());
        mUserTelephone.setText(ApplicationClass.people.get(index).getTelephone().trim());
    }

    private void addUser(final String name, final String phone) {
        ApplicationClass.people.add(new User(name, phone));
        mEnterUserName.setText(null);
        mEnterUserPhone.setText(null);
    }
}
