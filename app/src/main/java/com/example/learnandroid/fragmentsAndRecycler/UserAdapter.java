package com.example.learnandroid.fragmentsAndRecycler;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.learnandroid.R;

import java.util.List;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserViewHolder> {

    private List<User> people;

    ItemClicked activity;

    public interface ItemClicked {
        void onItemClicked(int index);
    }

    public UserAdapter(Context context, List<User> people) {
        activity = (ItemClicked) context;
        this.people = people;
    }

    //Custom view holder will contain all fields and data to change from single-item.xml for row file
    public class UserViewHolder extends RecyclerView.ViewHolder {
        TextView mUserName;

        //set the view it uses to display its contents
        public UserViewHolder(View itemView) {
            super(itemView);
            mUserName = itemView.findViewById(R.id.user_name_itemlist);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    activity.onItemClicked(people.indexOf((User) v.getTag()));
                }
            });
        }
    }

    @NonNull
    @Override
    public UserAdapter.UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        /*1. Create link to the view (user_single_itemlist.xml)
             we need to use LayoutInflater
             From (activity_display.xml (Parent)) to (user_single_itemlist.xml())*/
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_single_itemlist, parent, false);

         /*2. Pass this VIEW to Custom ViewHolder constructor (UserViewHolder) to link them
              Set the view by inflating an XML layout file*/
        return new UserViewHolder(view);
    }

    /**
     * This method runs for every element in the list, to populate the data
     *
     * @param viewHolder custom view holder
     * @param position   of the element in the list
     */
    @Override
    public void onBindViewHolder(@NonNull UserAdapter.UserViewHolder viewHolder, int position) {
        //itemView is basically whole layout element
        viewHolder.itemView.setTag(people.get(position));
        viewHolder.mUserName.setText(people.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return people.size();
    }
}
