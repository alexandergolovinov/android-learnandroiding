package com.example.learnandroid.fragmentsAndRecycler;

import android.app.Application;

import java.util.ArrayList;
import java.util.List;

/**
 * Class extending Application becomes the Application class itself for the app.
 * Declare this class in Manifest file as a name in <application tag>
 * android:name=".fragmentsAndRecycler.ApplicationClass"
 *
 * This class is the first class to run when the application starts
 */
public class ApplicationClass extends Application {

    public static List<User> people;

    @Override
    public void onCreate() {
        super.onCreate();
        people = new ArrayList<>();
        people.add(new User("Alexander", "523321122"));
        people.add(new User("Boris", "5321122"));
        people.add(new User("Oleg", "12323321122"));
        people.add(new User("Vasilij", "523321122"));
        people.add(new User("Nikola", "3213321122"));
        people.add(new User("Vika", "423221122"));
        people.add(new User("John", "523321122"));
        people.add(new User("Nargiza", "523321122"));
        people.add(new User("Maria", "53321122"));
        people.add(new User("Alexander", "523321122"));
        people.add(new User("Boris", "5321122"));
        people.add(new User("Oleg", "12323321122"));
        people.add(new User("Vasilij", "523321122"));
        people.add(new User("Nikola", "3213321122"));
        people.add(new User("Vika", "423221122"));
        people.add(new User("John", "523321122"));
        people.add(new User("Nargiza", "523321122"));
        people.add(new User("Maria", "53321122"));
        people.add(new User("Alexander", "523321122"));
        people.add(new User("Boris", "5321122"));
        people.add(new User("Oleg", "12323321122"));
        people.add(new User("Vasilij", "523321122"));
        people.add(new User("Nikola", "3213321122"));
        people.add(new User("Vika", "423221122"));
        people.add(new User("John", "523321122"));
        people.add(new User("Nargiza", "523321122"));
        people.add(new User("Maria", "53321122"));
    }
}
