package com.example.learnandroid.fragmentsAndRecycler;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.learnandroid.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListFrag extends Fragment {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter userAdapter;    //It is linked to UserAdapter, because UserAdapter extends from RecyclerView.Adapter
    private RecyclerView.LayoutManager layoutManager;
    private View view;


    public ListFrag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_list1, container, false);

        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        recyclerView = view.findViewById(R.id.list_recycle);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this.getActivity());
        recyclerView.setLayoutManager(layoutManager);

        userAdapter = new UserAdapter(this.getActivity(), ApplicationClass.people);
        recyclerView.setAdapter(userAdapter);
    }

    public void notifyDataChanged() {
        userAdapter.notifyDataSetChanged();
    }
}
