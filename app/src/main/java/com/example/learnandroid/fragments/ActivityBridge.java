package com.example.learnandroid.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.widget.TextView;

import com.example.learnandroid.R;

import java.util.ArrayList;
import java.util.List;

public class ActivityBridge extends FragmentActivity implements ListFragment.ItemSelected {

    TextView mTextDescription;
    List<String> dataDescriptions;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTextDescription = (TextView) findViewById(R.id.text_description);
        dataDescriptions = new ArrayList<>();
        dataDescriptions.add("one");
        dataDescriptions.add("two");
        dataDescriptions.add("three");
    }

    @Override
    public void onItemSelected(int index) {
        mTextDescription.setText(dataDescriptions.get(index));
    }
}
