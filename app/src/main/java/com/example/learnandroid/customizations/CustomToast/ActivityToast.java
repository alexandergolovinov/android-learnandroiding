package com.example.learnandroid.customizations.CustomToast;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.learnandroid.R;

public class ActivityToast extends Activity {

    private Button btnShowToast;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_toast);

        btnShowToast = (Button) findViewById(R.id.btn_show_toast);

        btnShowToast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showToast("My Custom Toast is displayed and Data saved succesfully!");
            }
        });
    }


    public void showToast(final String message) {
        //Inflater get the connection to our custom layout from the layout group
        View toastView = getLayoutInflater().inflate(R.layout.toast, (ViewGroup) findViewById(R.id.toast_layout));
        TextView toastText = (TextView) toastView.findViewById(R.id.toast_text);
        toastText.setText(message);

        Toast toast = new Toast(this.getApplicationContext());
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(toastView);
        toast.setGravity(Gravity.BOTTOM, 0, 510);
        toast.show();
    }

}
