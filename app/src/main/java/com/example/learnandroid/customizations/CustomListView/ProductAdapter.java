package com.example.learnandroid.customizations.CustomListView;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.learnandroid.R;

import java.util.List;

public class ProductAdapter extends ArrayAdapter<Product> {

    private final Context context;
    private final List<Product> products;


    public ProductAdapter(@NonNull Context context, List<Product> list) {
        super(context, R.layout.single_item_customlistview, list);
        this.context = context;
        this.products = list;
    }


    @NonNull
    @Override //this method runs for every item in the list
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.single_item_customlistview, parent, false);

        TextView productTitle = (TextView) rowView.findViewById(R.id.product_title);
        TextView productPrice = (TextView) rowView.findViewById(R.id.product_price);
        TextView productDescription = (TextView) rowView.findViewById(R.id.product_description);

        ImageView imageProduct = (ImageView) rowView.findViewById(R.id.product_image);
        ImageView imageProductAdd = (ImageView) rowView.findViewById(R.id.product_image_add);

        productTitle.setText(products.get(position).getTitle());
        productPrice.setText(products.get(position).getPrice() + "");
        productDescription.setText(products.get(position).getDescription());

        boolean isOnSale = products.get(position).isSale();
        if (isOnSale) {
            imageProductAdd.setImageResource(R.drawable.on_sale_big);
        } else {
            imageProductAdd.setImageResource(R.drawable.best_price);
        }

        String productType = products.get(position).getType();

        if (productType.equals("Laptop")) {
            imageProduct.setImageResource(R.drawable.laptop);
        } else if (productType.equals("Memory")) {
            imageProduct.setImageResource(R.drawable.memory);
        } else if (productType.equals("Screen")) {
            imageProduct.setImageResource(R.drawable.screen);
        } else if (productType.equals("hdd")) {
            imageProduct.setImageResource(R.drawable.hdd);
        }

        return rowView;
    }
}
