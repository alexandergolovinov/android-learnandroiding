package com.example.learnandroid.customizations.CustomListView;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.learnandroid.R;

import java.util.ArrayList;
import java.util.List;

public class ActivityLayoutListVIew extends Activity {

    private ListView mListView;
    List<Product> mProductList;
    ArrayAdapter<Product> productAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_listview);

        mProductList = new ArrayList<>();

        Product product1 = new Product("Dell Latitude 3500",
                "The world most secure laptop",
                "Laptop",
                14500.00,
                true);
        mProductList.add(product1);

        Product product2 = new Product("Apple Screen",
                "One of the most fancy screens Apple has",
                "Screen",
                19500.00,
                true);
        mProductList.add(product2);

        Product product3 = new Product("SANDISK Memory 5G",
                "5G of memory plus unlimited pool of strings",
                "Memory",
                500.00,
                false);
        mProductList.add(product3);

        Product product4 = new Product("HardDrive",
                "The world modst secure laptop",
                "hdd",
                899.99,
                true);
        mProductList.add(product4);

        Product product5 = new Product("Dell Latitude 3500",
                "The world most secure laptop",
                "Laptop",
                14500.00,
                true);
        mProductList.add(product5);

        mListView = (ListView) findViewById(R.id.custom_listView);


        productAdapter = new ProductAdapter(this, mProductList);
        mListView.setAdapter(productAdapter);


    }
}
