package com.example.learnandroid.customizations.CustomTextFields;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import com.example.learnandroid.R;

public class ActivityLayout extends Activity {

    private AutoCompleteTextView inputFirstName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_layout_customizations);

        inputFirstName = findViewById(R.id.input_firstname);
        //If user start to type from J, then show him this list
        String [] names = {"James" , "John", "Jenine", "Jack", "Johny"};

        //android.R.layout is a android own library with premade layouts.
        //ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, names);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.custom_design_autocomplete, names);

        //After one character show list of items.
        inputFirstName.setThreshold(1);
        inputFirstName.setAdapter(adapter);


    }
}
