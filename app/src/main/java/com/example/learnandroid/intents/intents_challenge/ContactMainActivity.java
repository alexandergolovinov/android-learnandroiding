package com.example.learnandroid.intents.intents_challenge;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.learnandroid.R;

public class ContactMainActivity extends Activity {

    private static final int INTENT_ONE = 1;

    Button btnCreateContact;

    TextView mName, mNumber, mLocation, mWeb;
    ImageView ivView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.challenge_main_activity);

        btnCreateContact = (Button) findViewById(R.id.btn_createContact);
        mName = (TextView) findViewById(R.id.nameMain);
        mNumber = (TextView) findViewById(R.id.numberMain);
        mLocation = (TextView) findViewById(R.id.locationMain);
        mWeb = (TextView) findViewById(R.id.websiteMain);
        ivView = (ImageView) findViewById(R.id.main_image);

        btnCreateContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ContactMainActivity.this, ContactFormActivity.class);
                startActivityForResult(intent, INTENT_ONE);
            }
        });

        mNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + mNumber.getText()));
                startActivity(intent);
            }
        });

        mWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = mWeb.getText().toString();
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://" + url));
                startActivity(intent);
            }
        });

        mLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:0,0?q=" + mLocation.getText().toString().trim()));
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == INTENT_ONE) {
            if (resultCode == RESULT_OK) {
                String name = data.getStringExtra("name");
                String number = data.getStringExtra("number");
                String website = data.getStringExtra("website");
                String location = data.getStringExtra("location");

                mName.setText(name);
                mNumber.setText(number);
                mLocation.setText(location);
                mWeb.setText(website);

            }

            if (resultCode == RESULT_CANCELED) {

            }
        }
    }
}
