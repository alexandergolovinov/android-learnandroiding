package com.example.learnandroid.intents.intents_challenge;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.learnandroid.R;

public class ContactFormActivity extends Activity {

    EditText mName, mNumber, mWebsite, mLocation;
    ImageView mHappy, mSad, mNeutral;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.challenge_form_activity);

        mName = (EditText) findViewById(R.id.name);
        mNumber = (EditText) findViewById(R.id.number);
        mWebsite = (EditText) findViewById(R.id.website);
        mLocation = (EditText) findViewById(R.id.location);

        mHappy = (ImageView) findViewById(R.id.imageView);

        mSad = (ImageView) findViewById(R.id.imageView2);
        mNeutral = (ImageView) findViewById(R.id.imageView3);

        mHappy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickIcon();
            }
        });

        mSad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickIcon();
            }
        });

        mNeutral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickIcon();
            }
        });
    }

    public void onClickIcon() {

        Intent intent = new Intent();
        String name = mName.getText().toString();
        String number = mNumber.getText().toString();
        String website = mWebsite.getText().toString();
        String location = mLocation.getText().toString();
        if (TextUtils.isEmpty(name) || TextUtils.isEmpty(number) || TextUtils.isEmpty(website) || TextUtils.isEmpty(location)) {
            Toast.makeText(this, "All fields should be filled", Toast.LENGTH_LONG).show();
        } else {
            intent.putExtra("name", name);
            intent.putExtra("number", number);
            intent.putExtra("website", website);
            intent.putExtra("location", location);

            setResult(RESULT_OK, intent);
            ContactFormActivity.this.finish();
        }

    }


}
