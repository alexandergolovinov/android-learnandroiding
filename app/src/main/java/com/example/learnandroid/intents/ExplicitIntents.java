package com.example.learnandroid.intents;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.learnandroid.R;

public class ExplicitIntents extends Activity {

    private static final int INTENT_ONE = 1;

    Button btn1;
    Button btn2;
    TextView mTextView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.explicit_intent);

        btn1 = (Button) findViewById(R.id.btn_explicit);
        btn2 = (Button) findViewById(R.id.btn_explicit2);
        mTextView = (TextView) findViewById(R.id.textView_main);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ExplicitIntents.this, ExplicitIntents.class);
                intent.putExtra("name", "Alexander");
                startActivity(intent);
            }
        });

        //StartActivityForResult returns certain result back.
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ExplicitIntents.this, ExplicitIntents.class);
                startActivityForResult(intent, INTENT_ONE);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == INTENT_ONE) {
            if (resultCode == RESULT_OK) {
                String textFromActivity2 = data.getStringExtra("text");
                mTextView.setText(textFromActivity2);
            }

            if (resultCode == RESULT_CANCELED) {
                mTextView.setText(R.string.text_cancel_status);
            }
        }
    }
}
