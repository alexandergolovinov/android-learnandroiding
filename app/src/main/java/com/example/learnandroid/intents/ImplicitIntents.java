package com.example.learnandroid.intents;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;

import com.example.learnandroid.R;

public class ImplicitIntents extends Activity {

    Button btnCall, btnCallFriend, btnOpenWeb, btnOpenMap;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.implicit_intent);

        btnCall = (Button) findViewById(R.id.btn_call);
        btnCallFriend = (Button) findViewById(R.id.btn_call_frd);
        btnOpenWeb = (Button) findViewById(R.id.btn_web);
        btnOpenMap = (Button) findViewById(R.id.btn_map);

        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                startActivity(intent);
            }
        });

        btnCallFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel: 445566"));
                startActivity(intent);
            }
        });

        btnOpenMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:0,0?q=-127.0, 64.0"));
                startActivity(intent);
            }
        });

        btnOpenWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.android.com/"));
                startActivity(intent);
            }
        });
    }
}
